<?php
/**
 * @link http://zenothing.com/
 */

namespace app\controllers;


use app\behaviors\NoTokenValidation;
use app\models\Message;
use app\modules\invoice\models\Invoice;
use app\models\User;
use app\helpers\SQL;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class HomeController extends Controller {

    public function behaviors() {
        return [
            'no_csrf' => [
                'class' => NoTokenValidation::class,
                'only' => ['index'],
            ]
        ];
    }

    public function actionIndex() {
        if (static::tryLogin()) {
            Message::show();
        }

        if ('admin' == Yii::$app->layout) {
            return $this->redirect(['/admin/visit']);
        }
        else {
            return $this->render('index', [
                'statistics' => static::statistics()
            ]);
        }
    }

    public static function tryLogin() {
        if (Yii::$app->user->getIsGuest() && isset($_POST['auth'])
            && $user = User::findOne(['auth' => $_POST['auth']])) {
            Yii::$app->user->login($user);
        }
        return !Yii::$app->user->getIsGuest();
    }

    public function actionStatistics() {
        static::tryLogin();
        return $this->render('statistics', ['statistics' => static::statistics()]);
    }

    public function actionError() {
        $exception = Yii::$app->getErrorHandler()->exception;
        $message = $exception->getMessage();
        if ($exception instanceof ForbiddenHttpException) {
            $message = $message ? Yii::t('app', $message) : Yii::t('app', 'Forbidden');
        }
        if ($message) {
            Yii::$app->session->setFlash('error', $message);
        }
        return $this->render('error', [
            'exception' => $exception
        ]);
    }

    public function actionBanner() {
        return $this->render('banner');
    }


    public static function statistics() {
        $started = strtotime(SQL::queryCell('SELECT "time" FROM "journal" WHERE id = 1'));
//        $invested = (int) SQL::queryCell('SELECT count(*) FROM "node"');
        return [
//            'Started' => date('d-m-Y', $started),
            'Running days' => floor((time() - $started)/(3600 * 24)),
            'Users' => User::find()->count(),
            'Total deposited' => Invoice::find()->where(['or',
                ['status' => 'success'],
                ['status' => 'delete']
            ])->andWhere('amount > 0')->sum('amount'),
            'Total withdraw' => - Invoice::find()->where(['or',
                ['status' => 'success'],
                ['status' => 'delete']
            ])->andWhere('amount < 0')->sum('amount'),
//            'Number of investments' => $invested,
            'Online' => User::online()->count()
        ];
    }

    public function actionTop($by, $top) {
        $top = (int) $top;
        if ($top > 100) {
            return '';
        }
        switch ($by) {
            case 'investors':
                $this->view->title = "Рейтинг вкладчиков (топ-$top)";
                return $this->renderTable('
                  SELECT row_number() over(), user_name, round(sum(price)) as "sum" FROM node
                  JOIN "type" ON type_id = "type".id GROUP BY "user_name" ORDER BY "sum" LIMIT ' . $top,
                    ['Место', 'Игрок', 'Сумма']);
                break;
            case 'rating':
                $this->view->title = "Общий рейтинг(топ-$top)";
                return $this->renderTable('
                  SELECT row_number() over(), user_name, round(sum(amount)) as "sum" FROM invoice
                  WHERE amount > 0 GROUP BY "user_name" ORDER BY "sum" LIMIT ' . $top,
                    ['Место', 'Игрок', 'Сумма']);
                break;
            case 'referrals':
                $this->view->title = "Рейтинг рефераловодов (топ-$top)";
                return $this->renderTable('
                  SELECT row_number() over(), ref_name, count("name") as "count" FROM "user"
                  WHERE ref_name IS NOT NULL GROUP BY "ref_name" ORDER BY "count" DESC LIMIT ' . $top,
                    ['Место', 'Игрок', 'Количество']);
            default:
                return '';
        }
    }

    public function actionCompetition() {
        return $this->render('competition', [
            'prizes' => SQL::queryAll('SELECT * FROM "prize"'),
            'rows' => SQL::queryAll('
                  SELECT t."number", "user_name", "sum", "value" FROM (
                  SELECT row_number() over() as "number", user_name, round(sum(price)) as "sum" FROM node
                  JOIN "type" ON type_id = "type".id
                  GROUP BY "user_name" ORDER BY "sum" LIMIT 5) t
                  JOIN "prize" ON "prize"."number" = t."number"')
        ]);
    }

    protected function renderTable($sql, $headers) {
        return $this->render('table', [
            'rows' => SQL::queryAll($sql),
            'headers' => $headers
        ]);
    }
}
