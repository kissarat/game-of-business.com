<?php
/**
 * @link http://zenothing.com/
 */

namespace app\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\models\Venture;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class VentureController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['take'],
            ]
        ];
    }

    public function actionIndex() {
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => Venture::find()->where(['status' => 1]),
                'sort' => [
                    'defaultOrder' => ['time' => SORT_DESC]
                ]
            ])
        ]);
    }

    public function actionTake($id = null) {
        /** @var \app\models\User $user */
        $transaction = Yii::$app->db->beginTransaction();
        $user = Yii::$app->user->identity;
        if ($id) {
            $model = Venture::findOne([
                'id' => $id,
                'status' => 1
            ]);
            if (!$model) {
                throw new ForbiddenHttpException();
            }
        }
        else {
            $model = new Venture([
                'user_name' => $user->name,
                'stake' => 10
            ]);
            if (!($model->load(Yii::$app->request->post()) && $model->validate())) {
                $transaction->rollBack();
                return $this->render('take', [
                    'model' => $model
                ]);
            }
        }

        if ($model->stake <= $user->account) {
            if (!$id) {
                $id = SQL::queryCell('INSERT INTO venture(stake) VALUES (:stake) RETURNING id', [':stake' => $model->stake]);
            }
            SQL::query('INSERT INTO venture_user(venture_id, user_name) VALUES (:venture_id, :user_name)', [
                ':venture_id' => $id,
                ':user_name' => $user->name,
            ]);
            $count = SQL::queryCell('SELECT count(*) FROM venture_user WHERE venture_id = :id', [':id' => $id]);
            if ($count >= 2) {
                $winner = SQL::queryCell('
                          SELECT user_name FROM venture_user WHERE venture_id = :id
                          ORDER BY random() LIMIT 1', [':id' => $id]);
                SQL::query('UPDATE "user" SET account = account + :amount WHERE "name" = :name', [
                    ':name' => $winner,
                    ':amount' => $model->stake * $count
                ]);
                SQL::query('UPDATE venture SET status = 0 WHERE id = :id', [':id' => $id]);
                Yii::$app->session->setFlash('success', "Ваша ставка $model->stake монет прийнята");
                $stake = $model->stake * $count;
                if ($winner == $user->name) {
                    Yii::$app->session->setFlash('success', "Вы выиграли $stake монет!");
                }
                else {
                    Yii::$app->session->setFlash('warning', 'К сожелению вы проиграли');
                }
            }
            else {
                Yii::$app->session->setFlash('info', "Ваша ставка $model->stake монет прийнята");
            }
            $transaction->commit();
            return $this->redirect(['index']);
        }
        else {
            $transaction->rollBack();
            $model->addError('stake', "На вашем счету только $model->stake монет");
        }
        return $this->render('take', [
            'model' => $model
        ]);
    }
}
