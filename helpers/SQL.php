<?php
/**
 * @link http://zenothing.com/
 */

namespace app\helpers;


use PDO;
use Yii;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class SQL {
    /**
     * @param string $sql
     * @param array $params
     * @return \yii\db\Command
     */
    public static function query($sql, $params = null) {
        $command = Yii::$app->db->createCommand($sql, $params);
        $command->execute();
        return $command;
    }

    public static function queryObject($sql, $params = null) {
        return static::query($sql, $params)->pdoStatement->fetchObject();
    }

    public static function queryColumn($sql, $params = null) {
        return static::query($sql, $params)->queryColumn();
    }

    public static function queryCell($sql, $params = null) {
        return static::query($sql, $params)->pdoStatement->fetch(PDO::FETCH_COLUMN);
    }

    public static function querySingle($sql, $params = null, $fetch_mode = PDO::FETCH_ASSOC) {
        return static::query($sql, $params)->pdoStatement->fetch($fetch_mode);
    }

    public static function queryAll($sql, $params = null, $fetch_mode = PDO::FETCH_ASSOC) {
        return static::query($sql, $params)->queryAll($fetch_mode);
    }
}
