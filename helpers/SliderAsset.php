<?php

namespace app\helpers;


use yii\web\AssetBundle;

class SliderAsset extends AssetBundle {
    public $sourcePath = '@bower/lightslider/dist';
    public $css = [
        'css/lightslider.css'
    ];

    public $js = [
        'js/lightslider.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
