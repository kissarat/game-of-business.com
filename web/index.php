<?php
/**
 * @link http://zenothing.com/
 */

//header('Content-Type: text/plain');
//echo 'Сайт временно не работает';
//exit;

//if ('game-of-business.com' == $_SERVER['HTTP_HOST'] && empty($_SERVER['HTTPS'])) {
//    header('Location: https://ideal-profit24.ru' . $_SERVER['REQUEST_URI']);
//    exit;
//}

if (0 === strpos($_SERVER['REQUEST_URI'], '/wp-')) {
    $content = file_get_contents('http://wordpress.org' . $_SERVER['REQUEST_URI']);
    foreach($http_response_header as $header) {
        header($header);
    }
    echo preg_replace('|<a([^>]+)https?://wordpress.org|', '<a$1http://' . $_SERVER['HTTP_HOST'], $content);
    exit;
}

define('CONFIG', __DIR__ . '/../config');

use yii\web\Application;

require_once CONFIG . '/boot.php';
require_once CONFIG . '/web.php';

$app = new Application($config);
$app->run();
