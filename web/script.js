"use strict";
/**
 * @link http://zenothing.com/
 * @author Taras Labiak <kissarat@gmail.com>
 */

var start = Date.now();

function $$(selector) { return document.querySelector(selector) }
function $id(id) { return document.getElementById(id) }
function $all(selector) { return document.querySelectorAll(selector) }
function $new(name) { return document.createElement(name) }
function $content(name, content) {
    var div = document.createElement(name);
    div.innerHTML = content;
    return div;
}
function $option(value, text) {
    var option = $new('option');
    option.value = value;
    option.innerHTML = text;
    return option;
}

function $a(label, url) {
    var a = $new('a');
    a.setAttribute('href', url);
    a.innerHTML = label;
    return a;
}

function $row() {
    var row = $new('tr');
    for(var i = 0; i < arguments.length; i++) {
        var cell = arguments[i];
        if (cell instanceof HTMLTableCellElement) {
            row.appendChild(cell);
        }
        else if (cell instanceof Element) {
            row.insertCell(-1).appendChild(cell);
        }
        else if ('string' == typeof cell) {
            row.insertCell(-1).innerHTML = cell;
        }
        else {
            row.insertCell(-1).innerHTML = cell.toString();
        }
    }
    return row;
}

function pg_time(string) {
    return moment(string, 'YYYY-MM-DD H:mm').format('DD-MM-YYYY H:M');
}

function array(a) {
    return Array.prototype.slice.call(a);
}

function $name(model, name) {
    $$(model + '[' + name + ']');
}

function $each(selector, call) {
    if ('string' == typeof selector) {
        selector = $all(selector);
    }
    return Array.prototype.forEach.call(selector, call);
}

function each(array, call) {
    Array.prototype.forEach.call(array, call);
}

function options(select, call) {
    $each(select.querySelectorAll('option'), call);
}

function script(source) {
    var element = document.createElement('script');
    element.setAttribute('src', source);
    document.head.appendChild(element);
}

var admin = 0 === location.hostname.indexOf('admin.');

//Feature detection
if (document.body.dataset) {
    $each('[data-selector]', function(source) {
        document.querySelector(source.dataset.selector).innerHTML = source.innerHTML;
        source.remove();
    })
}

var $duration = $$('[name="User[duration]"]');
var russian = /lang=en/.test(document.cookie) ? 0 : 1;

Object.defineProperty(Element.prototype, 'visible', {
    get: function() {
        return 'none' != this.style.display
    },
    set: function(value) {
        if (value) {
            this.style.removeProperty('display');
        }
        else {
            this.style.display = 'none';
        }
    }
});

var $remember = $$('[name="LoginForm[remember]"]');

if ($remember) {
    $$('[name="LoginForm[duration]"]').setAttribute('type', 'number');
    $('[name="LoginForm[remember]"]').change(function () {
        duration.visible = !duration.visible;
    });

    $('.user-login [name=select]').change(function () {
        if (!(duration_minutes.visible = '0' == this.value)) {
            $$('[name="LoginForm[duration]"]').value = this.value;
        }
    });
}

function browser(string) {
    return navigator.userAgent.indexOf(string) >= 0;
}

var $footer = $$('.footer');
if ($footer && browser('Windows')) {
    $footer.remove();
}

function report(async) {
    var request = new XMLHttpRequest();
    var params = {spend: window.performance ? performance.now() : Date.now() - start};
    if (window.user) {
        params['user'] = user;
    }
    var url = '/visit.php?' + $.param(params);
    request.open('GET', url, async);
    request.send(null);
}

function slide(slider) {
    var style = getComputedStyle(slider);
    var frames = slider.querySelectorAll('li');
    var left = 0;
    var width = parseFloat(style.width);
    $each(frames, function(frame) {
        frame.style.width = style.width;
        frame.style.height = style.height;
        frame.style.left = left + 'px';
        left -= width;
    });

    var queue = array(frames);

    setInterval(function() {
        $each(queue, function(frame) {
            frame.classList.add('animated');
            frame.style.left = (parseFloat(frame.style.left) + width) + 'px';
        });
        var part = queue.slice(1);
        left = 0;
        if (queue.length > 0) {
            queue[queue.length - 1].classList.remove('animated');
            $each(part, function(frame) {
                frame.style.left = left + 'px';
                left -= width;
            });
            part.push(queue[0]);
        }
        queue = part;
    }, 5000);
}

if (admin) {
    $('._blank').each(function(i, a) {
        a.setAttribute('target', '_blank');
    })
}
else {
    addEventListener('beforeunload', report);

    $each('[data-role="manage"]', function(element) {
        element.remove();
    });

    if (window.fullname) {
        var surname = $('.field-user-surname');
        var forename = $('.field-user-forename');
        var patronymic = $('.field-user-patronymic');
        fullname.onchange = function () {
            var names = $(this).find('input').val().split(/\s+/);
            surname.find('input').val(names[0] || '');
            forename.find('input').val(names[1] || '');
            patronymic.find('input').val(names[2] || '');
        };
        fullname.style.removeProperty('display');
        surname.css('display', 'none');
        forename.css('display', 'none');
        patronymic.css('display', 'none');
    }

    $('.glyphicon').click(function(e) {
        $(e.target).toggleClass('glyphicon-ok')
    });

    var tbody = $$('.venture.index tbody');
    var participants = $all('.participant div');
    if (tbody || participants) {
        var socket = new WebSocket('ws://0.0.0.0:5000/');
        socket.onmessage = function (e) {
            var message = JSON.parse(e.data);
            var row = $id(message.id);
            if (message._method) {
                switch (message._method) {
                    case 'insert':
                        var empty = tbody.querySelector('.empty');
                        if (empty) {
                            tbody.innerHTML = '';
                        }
                        row = $row(
                            message.user_name,
                            message.stake,
                            pg_time(message.time),
                            $a('Начать игру', 'take?id=' + message.id)
                        );
                        row.id = message.id;
                        tbody.appendChild(row);
                        break;
                    case 'update':
                        if (row) {
                            row.remove();
                        }
                        break;
                }
            }
            else {
                each(participants, function(p) {
                    if (message.number == p.innerHTML) {
                        p.parentNode.classList.add('busy');
                        p.parentNode.removeAttribute('href');
                        p.nextSibling.innerHTML = message.user_name;
                    }
                });
            }
        };
    }

    $each('.slider', slide);
}

if (window.localStorage) {
    if (!localStorage.getItem('first')) {
        localStorage.setItem('first', new Date().toISOString());
    }

    var $linux = $$('#linux');
    if ($linux && !localStorage.getItem('linux') && browser('Linux') && !browser('Android')) {
        $linux.onclick = function () {
            localStorage.setItem('linux', true);
            this.remove();
        };
        if (browser('Ubuntu')) {
            $linux.querySelector('img').setAttribute('src', '/images/ubuntu.png');
            var $welcome = $linux.querySelector('.welcome');
            $welcome.innerHTML = $welcome.innerHTML.replace('Linux', 'Ubuntu');
        }
        $linux.style.display = 'table-row';
    }
}

var units = {
    1: ['minutes', 'минут'],
    60: ['hours', 'часов'],
    1440: ['days', 'суток'],
    10080: ['weeks', 'недель'],
    43200: ['months', 'месяцев']
};

if (!admin) {
// Google Analytics
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-58031952-9', 'auto');
    ga('require', 'linkid', 'linkid.js');
    ga('send', 'pageview');

// Yandex.Metrika
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter32410265 = new Ya.Metrika({
                    id: 32410265,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");

    var metrika = $$('#metrika img');
    if (metrika) {
        metrika.onclick = function () {
            Ya.Metrika.informer({
                id: 32410265,
                lang: 'ru',
                i: this
            });
            return false
        };
    }
}
