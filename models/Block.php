<?php
/**
 * @link http://zenothing.com/
 */

namespace app\models;
use yii\db\ActiveRecord;
use Yii;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class Block extends ActiveRecord {

    public function primaryKeys() {
        return ['ip'];
    }

    public function rules() {
        return [
            ['ip', 'required'],
            [['ip', 'reason'], 'string', 'min' => 3],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'ip' => 'IP',
            'reason' => Yii::t('app', 'Reason'),
            'time' => Yii::t('app', 'Time'),
        ];
    }

    public static function isBlocked($ip) {
        return static::find()->where(['ip' => $ip])->count() > 0;
    }
}
