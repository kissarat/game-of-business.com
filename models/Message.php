<?php
/**
 * @link http://zenothing.com/
 */

namespace app\models;


use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * @property integer id
 * @property string sender
 * @property string receiver
 * @property string type
 * @property string link
 * @property string content
 * @property integer time
 */
class Message extends ActiveRecord {

    public static function tableName() {
        return 'note';
    }

    public static function &types() {
        $types = [];
        foreach(['info', 'success', 'warning', 'danger'] as $type) {
            $types[$type] = Yii::t('app', $type);
        }
        return $types;
    }

    public function rules() {
        return [
            [['receiver', 'type', 'content'], 'required'],
            ['receiver', 'exist',
                'targetClass' => 'app\models\User',
            'targetAttribute' => 'name'],
            ['type', 'in', 'range' => array_keys(static::types())],
            ['link', 'string', 'min' => 5],
            ['content', 'string', 'min' => 5]
        ];
    }

    public function attributeLabels() {
        return [
            'receiver' => Yii::t('app', 'Receiver'),
            'type' => Yii::t('app', 'Type'),
            'link' => Yii::t('app', 'URL'),
            'content' => Yii::t('app', 'Content'),
        ];
    }

    /**
     * @param string $receiver
     * @param string $type
     * @param string $content
     * @param string|array $link
     * @return Message
     */
    public static function send($receiver, $type, $content, $link = null) {
        $message = new Message([
            'receiver' => $receiver,
            'type' => $type,
            'content' => $content,
            'link' => is_array($link) ? Url::to($link) : $link
        ]);
        $message->save(false);
        return $message;
    }

    public static function show() {
        /** @var Message[] $messages */
        $messages = Message::find()
            ->where(['receiver' => Yii::$app->user->identity->name])
            ->orderBy(['id' => SORT_ASC])
            ->all();

        foreach($messages as $message) {
            $content = Yii::t('app', $message->content);
            if ($message->link) {
                $content .= ' 1 ' . Html::a(Yii::t('app', 'Show'), $message->link);
            }
            Yii::$app->session->addFlash($message->type, $content);
            $message->delete();
        }
    }
}
