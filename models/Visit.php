<?php
/**
 * @link http://zenothing.com/
 */

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class Visit extends ActiveRecord {
    public function attributeLabels() {
        return [
            'time' => Yii::t('app', 'Time'),
            'spend' => Yii::t('app', 'Spend'),
            'user_name' => Yii::t('app', 'Username'),
            'path' => Yii::t('app', 'Path'),
            'ip' => 'IP',
            'agent' => Yii::t('app', 'Browser'),
        ];
    }
}
