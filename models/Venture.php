<?php
/**
 * @link http://zenothing.com/
 */

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

class Venture extends ActiveRecord {

    public function rules() {
        return [
            ['id', 'integer'],
            ['stake', 'required'],
            ['user_name', 'string'],
            ['status', 'integer'],
            ['stake', 'integer', 'min' => 1, 'max' => 50]
        ];
    }

    public static function tableName() {
        return 'venture_view';
    }

    public function attributeLabels() {
        return [
            'user_name' => Yii::t('app', 'Игрок'),
            'stake' => Yii::t('app', 'Ставка'),
            'time' => Yii::t('app', 'Дата'),
        ];
    }
}
