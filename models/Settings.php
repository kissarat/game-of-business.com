<?php

namespace app\models;


use PDO;
use Yii;
use yii\base\Model;

class Settings extends Model {
    const CACHE_KEY = 'settings';

    private $_attributes = [];
    public $category;
    private static $_all;

    public static function all() {
        if (!static::$_all) {
//            static::$_all = Yii::$app->cache->get(static::CACHE_KEY);
//            if (!static::$_all) {
                static::$_all = [];
                $command = Yii::$app->db->createCommand('SELECT * FROM setting');
                $command->execute();
                while($setting = $command->pdoStatement->fetch(PDO::FETCH_NUM)) {
                    static::$_all[$setting[0]][$setting[1]] = $setting[2];
                }
                Yii::$app->cache->set(static::CACHE_KEY, static::$_all);
//            }
        } 
        return static::$_all;
    }

    public static function get($category, $name) {
        return static::all()[$category][$name];
    }

    public static function getCategory($name) {
        return static::all()[$name];
    }

    public function setup($attributes) {
        foreach($attributes as $name => $value) {
            $this->$name = $value;
        }
    }

    public static function set($category, $name, $value, $reset = true) {
        $command = Yii::$app->db->createCommand('
          UPDATE "setting" SET "value" = :value
          WHERE "category" = :category AND "name" = :name', [
            ':category' => $category,
            ':name' => $name,
            ':value' => $value
        ]);
        $count = $command->execute();
        if ($count && $reset) {
            Yii::$app->cache->delete('settings');
        }
        return $count;
    }

    public function save() {
        $count = 0;
        foreach($this->_attributes as $name => $value) {
            $count += Settings::set($this->category, $name, $value, false);
        }
        Yii::$app->cache->delete('settings');
        return $count;
    }

    public static function categories() {
        return [
            'common' => Yii::t('app', 'Общие'),
            'perfect' => Yii::t('app', 'Perfect Money'),
            'account' => Yii::t('app', 'Счета')
        ];
    }

    public function attributes() {
        $names = array_keys(static::getCategory($this->category));
        $attributes = [];
        foreach($names as $name) {
            $attributes[] = $this->category . '_' . $name;
        }
        return $attributes;
    }

    public function attributeName($name) {
        return substr($name, strlen($this->category) + 1);
    }

    public function initAttributes() {
        $this->_attributes = $this->getCategory($this->category);
    }

    public function getTitle() {
        return static::categories()[$this->category];
    }

    public function __get($name) {
        return $this->_attributes[$this->attributeName($name)];
    }

    public function __set($name, $value) {
        $this->_attributes[$this->attributeName($name)] = $value;
    }

    public function attributeLabels() {
        return [
            'perfect_id' => 'ID',
            'perfect_password' => Yii::t('app', 'Пароль'),
            'perfect_wallet' => Yii::t('app', 'Кошелек'),
            'perfect_alternate_secret' => Yii::t('app', 'Альтернативний код'),

            'account_balance' => Yii::t('app', 'Баланс при регистрации'),
            'account_min_payment' => Yii::t('app', 'Минимальний платеж'),
            'account_max_withdraw' => Yii::t('app', 'Максимальний вывод'),
            'account_bonus' => Yii::t('app', 'Реферальний бонус'),
            'account_vip' => Yii::t('app', 'Стоимость VIP'),
            'account_rate' => Yii::t('app', 'Курс (количство монет, которих можно купить за $1)'),
            'account_return' => Yii::t('app', 'Возврат VIP-пользователям при покупке предприятия обратно на баланс'),
            'account_vip_duration' => Yii::t('app', 'Продолжительность VIP-статуса в днях'),
        ];
    }
}
