<?php
/**
 * @link http://zenothing.com/
 * @var Message model
 */

use app\models\Message;
use app\widgets\AjaxComplete;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Inform');

$form = ActiveForm::begin();
echo implode("\n", [
    $form->field($model, 'receiver')->widget(AjaxComplete::class, [
        'route' => ['user/complete']
    ]),
    $form->field($model, 'type')->dropDownList(Message::types()),
    $form->field($model, 'link'),
    $form->field($model, 'content')->textarea(),
    Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-primary'])
]);
ActiveForm::end();