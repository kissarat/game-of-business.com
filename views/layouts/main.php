<?php
/**
 * @link http://zenothing.com/
 */

use app\helpers\MainAsset;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

MainAsset::register($this);
$login = Yii::$app->user->isGuest ? '' : 'login';
$manager = !Yii::$app->user->isGuest && Yii::$app->user->identity->isManager();
$route = [Yii::$app->controller->id, Yii::$app->controller->action->id];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>"><!--

Домен зарегистрирован на имя разработчика и хоститься на его (разработчика) серверах,
но разработчик не являеться владельцем сайта и не несет ответственности за его действия,
т.е. разработчик не несет ответственности за действия владельца сайта,
которые, в том числе, включают в себя проведения любых денежных операций
связанных с кошельком Perfect Money U9060584
<?= "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n
\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" ?>
-->
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="image_src" href="/images/cover.png" />
    <?php $this->head() ?>
    <?= Html::csrfMetaTags() ?>
</head>
<body class="<?= implode(' ', $route) ?>">
<?php $this->beginBody();
if (!Yii::$app->user->getIsGuest()) {
    $user = Yii::$app->user->identity->name;
    echo Html::script("var user = '$user'");
}
?>
<header></header>
<div class="wrap <?= $login ?>">
    <?php
    NavBar::begin();

    $items = [
        ['label' => Yii::t('app', 'Home'), 'url' => ['/home/index'], 'options' => ['class' => 'hideable']],
        ['label' => Yii::t('app', 'Аукцион'), 'url' => ['/bank/lottery/index']],
        ['label' => Yii::t('app', 'News'), 'url' => ['/article/article/index']],
        ['label' => Yii::t('app', 'FAQ'), 'url' => ['/faq/faq/index']],
        ['label' => Yii::t('app', 'Конкурс'), 'url' => ['/home/competition']],
        ['label' => Yii::t('app', 'Invoices'), 'url' => ['/invoice/invoice/index']],
        ['label' => Yii::t('app', 'Rules'), 'url' => ['/article/article/page', 'name' => 'rules']],
    ];

    if ($manager) {
        $items[0] = ['label' => Yii::t('app', 'Admin Panel'),
            'url' => 'http://admin.' . $_SERVER['HTTP_HOST'] . '/bank/node/index',
            'options' => [
                'data' => [
                    'method' => 'post',
                    'params' => [
                        'auth' => Yii::$app->user->identity->auth
                    ]
                ]
            ]
        ];
    }

    $items[] = ['label' => Yii::t('app', 'Contacts'), 'url' => ['/article/article/page', 'name' => 'contacts']];

    if (Yii::$app->user->getIsGuest()) {
        $items[] = ['label' => Yii::t('app', 'Login'), 'url' => ['/user/login'], 'options' => ['class' => 'right']];
        $items[] = ['label' => Yii::t('app', 'Signup'), 'url' => ['/user/signup'], 'options' => ['class' => 'right']];
    }
    else {
        $items[] = ['label' => Yii::t('app', 'Logout'), 'url' => ['/user/logout'], 'options' => ['class' => 'right']];
        $items[] = ['label' => Yii::t('app', 'Profile'), 'url' => ['/user/cabinet'], 'options' => ['class' => 'right']];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => $items,
    ]);
    NavBar::end();
    ?>
    <div style="text-align: center">
        <?= Html::a('', ['/page/marketing'], ['class' => 'start']) ?>
    </div>

    <div class="container">
        <?= Breadcrumbs::widget([
            'homeLink' => false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>

    <!--    --><?php //require_once 'linux.php' ?>
    <footer class="container">
    </footer>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
