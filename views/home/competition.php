<?php
use yii\helpers\Html;

$items = [];
foreach($prizes as $prize) {
    $items[] = $prize['number'] . ' место - ' . $prize['value'] . ' рублей';
}
?>
<div>
    <h2>Описание конкурса:</h2>
    <p>
        Каждому участнику проекта предоставляется возможность стать участником ежемесячного
        конкурса инвесторов в нашем проекте. Для этого вам необходимо пополнить свой счёт на
        наибольшую сумму, по итогам конкурса награждаются 5 человек пополнившие баланс на максимальную сумму.
    </p>
    <strong>
        Акция действует с 03.09 по 03.10, призы победителям
        начислятся на след. день после окончания конкурса.
    </strong>
</div>
<div>
    <h2>Призы:</h2>
    <?= Html::ul($items) ?>
</div>

<?php
$headers = ['Место','Игрок','Сумма','Приз'];
require 'table.php';
