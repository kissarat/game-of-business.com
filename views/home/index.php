<?php
/**
 * @link http://zenothing.com/
 * @var string $statistics
 */

use app\helpers\SliderAsset;
use app\modules\article\models\Article;
use app\modules\bank\controllers\TypeController;
use yii\helpers\Html;

$this->title = Yii::$app->name;

SliderAsset::register($this);
?>
<div class="home-index">
    <div class="slider-container">
        <ul class="slider">
            <li>
                <img src="/images/slides/1.jpg" />
                <h3>Создай свой бизнес онлайн и <br>получай деньги в реальности!</h3>
            </li>
            <li>
                <h3>Бизнес игра game-of-business.com<br>
                    Ваш финансовый успех начинается сегодня
                    получай деньги в реальности!</h3>
                <img src="/images/slides/2.png" />
            </li>
            <li>
                <h3>Купите свою первую недвижимость<br>И начните зарабатывать прямо сейчас</h3>
                <img src="/images/slides/3.png" />
            </li>
        </ul>
    </div>
    <div class="statistics">
        <h3>Статистика</h3>
        <div>
            <div class="numbers">
                <div><p>ЗАРЕГИСТРИРОВАНО: <span><?= $statistics['Users'] ?></span></p></div>
                <div><p>ВЫПЛАЧЕНО ВСЕГО: <span><?= $statistics['Total withdraw'] ?></span></p></div>
                <div><p>ВЛОЖЕНО ВСЕГО: <span><?= $statistics['Total deposited'] ?></span></p></div>
                <div><p>ONLINE: <span><?= $statistics['Online'] ?></span></p></div>
            </div>
            <div class="days">
                <p>ДНЕЙ ПРОЕКТУ: <br/><span><?= $statistics['Running days'] ?></span></p>
            </div>
        </div>
    </div>
    <?= Yii::$app->view->renderFile('@app/modules/article/views/article/view.php', [
        'model' => Article::findOne(['name' => 'about'])
    ]) ?>

    <?= Yii::$app->view->renderFile('@app/modules/bank/views/type/index.php', TypeController::index()) ?>
</div>
