<?php
/**
 * @link http://zenothing.com/
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

echo Html::tag('h1', Yii::t('app', 'Block by IP'));
$form = ActiveForm::begin();
echo $form->field($model, 'ip');
echo $form->field($model, 'reason')->textarea();
echo Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']);
ActiveForm::end();
