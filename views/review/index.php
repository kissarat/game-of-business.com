<?php
/**
 * @link http://zenothing.com/
*/

use app\widgets\Ext;
use yii\helpers\Html;

/**
 * @var \app\models\Review[] $models
 */

$this->title = Yii::t('app', 'Reviews');
?>
<div class="review-index">
    <?= Ext::stamp() ?>

    <h1><?= $this->title ?></h1>
    <div class="form-group">
        <?php
        if (!Yii::$app->user->getIsGuest()) {
            echo Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']);
        }
        ?>
    </div>
    <dl>
        <?php foreach($models as $model): ?>
        <dt><?= Html::a($model->user_name, ['user/view', 'name' => $model->user_name]) ?></dt>
        <dd>
            <?= $model->content ?>
            <div class="form-group">
                <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isManager()): ?>
                    <?= Html::a(Yii::t('app', 'Update'),
                        ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']); ?>
                    <?= Html::a(Yii::t('app', 'Delete'),
                        ['delete', 'id' => $model->id], ['class' => 'btn btn-danger btn-sm']); ?>
                <?php endif; ?>
            </div>
        </dd>
        <?php endforeach; ?>
    </dl>
</div>
