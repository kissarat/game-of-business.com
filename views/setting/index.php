<?php
use yii\helpers\Html;

$items = [];
foreach($categories as $name => $label) {
    $items[] = Html::a($label, ['/setting/edit', 'category' => $name]);
}

echo Html::tag('h1', 'Настройки');
echo Html::ul($items, [
    'encode' => false
]);
