<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/** @var \app\models\Settings $model */
$this->title = $model->getTitle();
echo Html::tag('h1', $this->title);
$form = ActiveForm::begin();
foreach($model->attributes() as $name) {
    echo $form->field($model, $name);
}
echo Html::submitButton(Yii::t('app', 'Save'));
ActiveForm::end();
