<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Новая игра';
$form = ActiveForm::begin();
echo $form->field($model, 'stake');
echo Html::submitButton(Yii::t('app', 'Create'));
ActiveForm::end();
