<?php
/** @var \yii\data\ActiveDataProvider $dataProvider */
use app\models\Venture;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Текищие игры';
echo Html::a('Создать новую игру', ['take']);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'rowOptions' => function(Venture $model) {
        return [
            'id' => $model->id
        ];
    },
    'columns' => [
        'user_name',
        'stake',
        'time:datetime',
        [
            'format' => 'html',
            'value' => function(Venture $model) {
                return Html::a('Начать игру',
                    ['take', 'id' => $model->id],
                    [
                        'data-method' => 'post',
                        'encode' => false
                    ]);
            }
        ]
    ]
]);
