<?php
/**
 * @link http://zenothing.com/
 */

use app\widgets\Ext;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LoginForm */
/* @var $form ActiveForm */

$this->title = Yii::t('app', 'Login');
if ('admin' == Yii::$app->layout) {
    $this->title = Yii::t('app', 'Admin Panel') . ': ' . $this->title;
}
?>
<div class="user-login">
    <?= Ext::stamp() ?>
    <h1 class="blue-border"><?= $this->title ?></h1>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>

    <?php if ('admin' == Yii::$app->layout): ?>
        <div class="form-group">
            <?= Html::activeCheckbox($model, 'remember') ?>
            <span id="duration" style="display: none">
            <?= Html::dropDownList('select', $model->duration, [
                0 => Yii::t('app', 'other'),
                60 => Yii::t('app', 'hour'),
                60 * 24 => Yii::t('app', 'day'),
                60 * 24 * 7 => Yii::t('app', 'week'),
                60 * 24 * 30 => Yii::t('app', 'month'),
            ]) ?>
                <span id="duration_minutes" style="display: none">
                <?= Html::activeTextInput($model, 'duration') ?>
                <?= Yii::t('app', 'minutes') ?>
            </span>
        </span>
        </div>
    <?php endif ?>

    <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-primary']) ?>

    <?php if ('main' == Yii::$app->layout): ?>
        <div class="form-group">
            <?= Html::a('Забыли пароль?', ['request']) ?>
        </div>
    <?php endif ?>
    <?php ActiveForm::end(); ?>
</div><!-- user-login -->
