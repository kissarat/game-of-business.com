<?php
use app\models\Settings;
use yii\helpers\Html;
use yii\helpers\Url;

if (!Yii::$app->user->getIsGuest()):
    ?>
    <div id='user-panel'>
        <ul>
            <li> <?=  Html::a(Yii::t('app', 'Opened Deposits'),
                    ['/bank/node/index', 'user' => $model->name], ['class' => 'btn btn-primary']) ?></li>
            <li> <?= Html::a(Yii::t('app', 'Income'),
                    ['/bank/income/index', 'user' => $model->name], ['class' => 'btn btn-primary']) ?></li>
            <?php
            if (!Yii::$app->user->isGuest) {
                if ($model->name == Yii::$app->user->identity->name || Yii::$app->user->identity->isAdmin()) {
                    echo Html::tag('li', Html::a(Yii::t('app', 'Change Password'),
                        ['/user/password', 'name' => $model->name], ['class' => 'btn btn-warning']));
                }
                ?>
                <?php
                if (Yii::$app->user->identity->isManager()) {
                    echo Html::tag('li', Html::a(Yii::t('app', 'Inform'),
                        ['/message/create', 'user' => $model->name], ['class' => 'btn btn-primary']));
                }
                ?>
                <li> <?= Html::a(Yii::t('app', 'Update'), ['/user/update', 'name' => $model->name], ['class' => 'btn btn-primary']) ?></li>
                <?php

                if ($model->name == Yii::$app->user->identity->name) {
                    echo Html::tag('li', Html::a(Yii::t('app', 'Sponsors'),
                        ['index'], ['class' => 'btn btn-primary']));
                }

                if (Yii::$app->user->identity->isAdmin()) {
                    if (empty($model->hash)) {
                        echo Html::tag('li', Html::a(Yii::t('app', 'Activate'), ['/user/email', 'code' => $model->name], ['class' => 'btn btn-primary']));
                    }
                    echo Html::tag('li', Html::a(Yii::t('app', 'Delete'), ['/user/delete', 'name' => $model->name
                    ], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]));
                }
                elseif ($model->name == Yii::$app->user->identity->name) {
                    echo Html::tag('li', implode("\n", [
                        Html::beginForm('https://perfectmoney.is/api/step1.asp', 'POST'),
                        Html::hiddenInput('PAYEE_ACCOUNT', Settings::get('perfect', 'wallet')),
                        Html::textInput('PAYMENT_AMOUNT', 20, ['size' => 3]),
                        Html::hiddenInput('PAYEE_NAME', Yii::$app->name),
                        Html::hiddenInput('PAYMENT_UNITS', 'USD'),
                        Html::hiddenInput('PAYMENT_URL', Url::to(['/invoice/invoice/success'], true)),
                        Html::hiddenInput('NOPAYMENT_URL', Url::to(['/invoice/invoice/fail'], true)),
                        Html::hiddenInput('BAGGAGE_FIELDS', 'USER_NAME'),
                        Html::hiddenInput('USER_NAME', Yii::$app->user->identity->name),
                        Html::button(Yii::t('app', 'Pay'), [
                            'name' => 'PAYMENT_METHOD',
                            'type' => 'submit',
                            'class' => 'btn btn-primary'
                        ]),
                        Html::endForm()
                    ]));
                }
            }
            ?>
        </ul>
    </div>
<?php
endif;
