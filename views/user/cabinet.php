<?php
/** @var \app\models\User $model */

use app\models\Settings;
use yii\helpers\Html;

$max_withdraw = Settings::get('account', 'max_withdraw');
?>
<div class="cabinet">
    <h1 class="cabinet-title">
        <span>Кабинет</span>
        <a href="/user/password" class="settings">Настройки</a>
    </h1>
    <div class="profile-stats">
        <div class="red-buttons">
            <?= Html::a('Камень, ножницы, бумага', ['/venture/index'], ['class' => 'btn']) ?>
            <?= Html::a('Мои рефералы', ['/user/index'], ['class' => 'btn']) ?>
            <?= Html::a('История вывода средств', ['/invoice/invoice/index',
                'scenario' => 'withdraw', 'user' => $model->name],
                ['class' => 'btn']) ?>
            <?= Html::a('ТОП-10 инвесторов', ['/home/top', 'by' => 'investors', 'top' => 10],
                ['class' => 'btn']) ?>
            <?= Html::a('ТОП-10 рефераловодов', ['/home/top', 'by' => 'referrals', 'top' => 10],
                ['class' => 'btn']) ?>
            <?= Html::a('ТОП-50 рейтинг', ['/home/top', 'by' => 'rating', 'top' => 50],
                ['class' => 'btn']) ?>
        </div>

        <div align="center" class="orange-buttons">
            <a class="btn" href="/feedback/feedback/create">Служба поддержки</a>
        </div>
        <?php
        if ($model->getVip()):
            echo "<div><strong>VIP-статус</strong>: осталось " . $model->getVipDays() . " дней</div>";
        else:
            ?>
            <div class="vip">
                <div class="left">
                    <div class="title">Ваш статус: <strong>Обычный пользователь</strong></div>
                </div>
                <div class="right">
                    <div class="title">Купите VIP-статус и получите дополнительные преимущества:</div>
                    <ol>
                        <li>Возвращаем <?= Settings::get('account', 'return') * 100 ?>% при покупке предприятия обратно на баланс</li>
                        <li><?= Settings::get('account', 'bonus') * 100 ?>% реферальный бонус (для 1 уровня)</li>
                        <li>Отсутствие ежедневных лимитов на выплаты
                            (без VIP лимит <?= $max_withdraw ?> рублей/день)</li>
                    </ol>
                    <p>
                        Стоимость VIP: <?= Settings::get('account', 'vip') ?> монет. <br>
                        Срок действия: <?= Settings::get('account', 'vip_duration') ?> дней.</p>
                    <div class="orange-buttons">
                        <?= Html::a('Купить', ['/user/vip'], [
                            'class' => 'btn btn-success',
                            'data-method' => 'post'
                        ]) ?>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        <?php endif ?>

        <div class="footer">
            <div class="menu">
                <div class="row1">
                    <p>Баланс: <br><span><?= $model->account ?> монет</span></p>
                    <a class="btn" href="/">Купить бизнес</a>
                    <?= Html::a('Купить монеты', ['/invoice/invoice/create', 'scenario' => 'payment'],
                        ['class' => 'btn']) ?>
                </div>
                <div class="row2">
                    <p>Заработано: <br><span><?= $model->account ?> монет</span></p>
                    <?= Html::a('Вывести', ['/invoice/invoice/create', 'scenario' => 'withdraw'],
                        ['class' => 'btn']) ?>
                </div>
            </div>
            <div class="stats">
                <p>Статистика:</p>
                <div>
                    <table>
                        <tbody>
                        <tr>
                            <td>Бизнесов:</td>
                            <td><?= $firms->count() ?></td>
                        </tr>
                        <tr>
                            <td>Доход(в час):</td>
                            <td><?= floor($hourly * 1000) / 1000 ?> монет</td>
                        </tr>
                        <tr>
                            <td>Доход(в день): </td>
                            <td><?= floor($daily * 1000) / 1000 ?> монет</td>
                        </tr>
                        <tr>
                            <td>Заработано(всего):</td>
                            <td><?= round($profit, 3) ?> монет</td>
                        </tr>
                        <tr>
                            <td>Рефералы(всего):</td>
                            <td>0 руб.</td>
                        </tr>
                        <tr>
                            <td>Денег на вывод:</td>
                            <td><?= $model->account > $max_withdraw ? $max_withdraw : $model->account ?> руб.</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="my-business">
            <h2>Мой бизнес</h2>
        </div>

        <div class="business-shop">
            <?= Yii::$app->view->render('@app/modules/bank/views/node/firms', [
                'firms' => $firms->all()
            ]); ?>
        </div>
    </div>
</div>
