<?php
/**
 * @link http://zenothing.com/
 */

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', Yii::$app->user->identity->isManager() ? 'Users' : 'Sponsors');

$columns = [
    [
        'attribute' => 'name',
        'format' => 'html',
        'value' => function($model) {
            return Html::a($model->name, ['view', 'name' => $model->name]);
        }
    ],
    'account',
];

if (!Yii::$app->user->getIsGuest() && Yii::$app->user->identity->isAdmin()) {
    $columns[] = 'email:email';
    $columns[] = [
        'format' => 'html',
        'contentOptions' => ['class' => 'action'],
        'value' => function($model) {
            return implode(' ', [
                Html::a('', ['update', 'name' => $model->name], ['class' => 'glyphicon glyphicon-pencil']),
//                Html::a('', ['delete', 'id' => $model->id], ['class' => 'glyphicon glyphicon-trash'])
            ]);
        }
    ];
}
?>
<div class="user-index">
    <div>
        <h1 class="bagatelle"><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?php
            if (Yii::$app->user->identity->isAdmin()) {
                echo Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']);
            }
            ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $columns,
        ]); ?>
    </div>
</div>
