<?php
/**
 * @link http://zenothing.com/
 */

use app\models\User;
use app\widgets\Ext;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */

function submit($label) {
    return Html::submitButton($label, ['class' => 'button']);
}

echo Ext::stamp();

$form = ActiveForm::begin();

if ($model->ref_name && $model->isNewRecord): ?>
    <div class="form-group">
        <?= Yii::t('app', 'Referral') . ': ' . $model->ref_name; ?>
    </div>
    <?= Html::activeHiddenInput($model, 'ref_name') ?>
<?php endif;
if ('signup' == $model->scenario) {
    ?>
    <div id="fullname" class="form-group" style="display: none">
        <label>Ф.И.О</label>
        <input class="form-control" />
    </div>
    <?php
    echo $form->field($model, 'surname');
    echo $form->field($model, 'forename');
    echo $form->field($model, 'patronymic');
    echo $form->field($model, 'email');
    echo $form->field($model, 'perfect');
}

if ($model->isNewRecord) {
    echo $form->field($model, 'name');
}

if ('signup' == $model->scenario):
    echo $form->field($model, 'password')->passwordInput();
    echo $form->field($model, 'repeat')->passwordInput();
    ?>
    <div>
        <div class="always-true">
            <div>Я подтверждаю, что мне 18 лет</div>
            <div class="glyphicon"></div>
        </div>
        <div class="always-true">
            <div>Принять условие пользовательского соглашения</div>
            <div class="glyphicon"></div>
        </div>
    </div>
    <?php
endif;

if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin()) {
    echo $form->field($model, 'account');
    echo $form->field($model, 'status')->dropDownList(User::statuses());
}

?>
<div style="text-align: center">
    <?php

    if (Yii::$app->user->isGuest) {
        echo submit(Yii::t('app', 'Signup'));
    }
    else {
        echo submit(Yii::t('app', $model->isNewRecord ? 'Create' : 'Update'));
    }

    ActiveForm::end();
    ?>
</div>
