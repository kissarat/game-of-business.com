<?php
use app\models\Settings;
use yii\helpers\Html;
use yii\helpers\Url;



echo Html::beginForm('https://perfectmoney.is/api/step1.asp', 'POST');
echo Html::hiddenInput('PAYEE_ACCOUNT', Settings::get('perfect', 'wallet'));
echo Html::textInput('PAYMENT_AMOUNT', $model->amount);
echo Html::hiddenInput('PAYEE_NAME', Yii::$app->name);
echo Html::hiddenInput('PAYMENT_UNITS', 'USD');
echo Html::hiddenInput('STATUS_URL', Url::to(['/invoice/invoice/view'], true));
echo Html::hiddenInput('PAYMENT_URL', Url::to(['/invoice/invoice/success'], true));
echo Html::hiddenInput('NOPAYMENT_URL', Url::to(['/invoice/invoice/fail'], true));
echo Html::hiddenInput('BAGGAGE_FIELDS', 'USER_NAME');
echo Html::hiddenInput('USER_NAME', Yii::$app->user->identity->name);
echo Html::button(Yii::t('app', 'Продолжить'), [
    'name' => 'PAYMENT_METHOD',
    'type' => 'submit',
    'class' => 'btn btn-success'
]);
echo Html::endForm();
