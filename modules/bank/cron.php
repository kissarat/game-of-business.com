<?php
require_once __DIR__ . '/../../config/pdo.php';
require_once 'models/Type.php';

$pdo = connect();
$pdo->beginTransaction();

$pdo->exec('CREATE TEMPORARY TABLE cron AS
SELECT node.id, user_name, node.type_id, daily
FROM node LEFT JOIN income ON node.id = node_id
JOIN type_level t ON node.type_id = t.type_id
WHERE node.level_number <= t.number AND ((NOW() - node."time") > INTERVAL \'1 minutes\'
    OR (income.id IS NULL AND(NOW() - income."time") > INTERVAL \'1 minutes\'))
    GROUP BY node.id, t.type_id, daily ORDER BY node.id');

$pdo->exec('INSERT INTO income(node_id) (SELECT id FROM cron)');

$pdo->exec('UPDATE "user" SET account = account + income
FROM (SELECT user_name, sum(daily) as income FROM cron GROUP BY user_name) cron
WHERE "name" = user_name');

$pdo->exec('DROP TABLE cron');

$pdo->commit();
