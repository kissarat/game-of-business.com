<?php

namespace app\modules\bank\controllers;


use app\behaviors\Access;
use app\modules\bank\models\Level;
use app\modules\bank\models\Type;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class LevelController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => Access::class,
                'manager' => ['index', 'view', 'update', 'delete'],
            ]
        ];
    }

    public function actionIndex($type_id) {
        $firm = Type::findOne($type_id);
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => Level::find()->where(['type_id' => $type_id])
            ]),
            'firm' => $firm
        ]);
    }

    protected function edit(Level $model) {
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/bank/level/index', 'type_id' => $model->type_id]);
        }
        return $this->render('edit', [
            'model' => $model
        ]);
    }

    public function actionCreate($type_id) {
        $model = new Level([
            'type_id' => $type_id,
            'number' => 1,
            'income' => 0.05
        ]);
        $last = Level::find()
            ->where(['type_id' => $type_id])
            ->orderBy(['number' => SORT_DESC])
            ->one();
        if ($last) {
            $model->number = $last->number + 1;
            $model->income = $last->income;
        }
        return $this->edit($model);
    }

    public function actionUpdate($id) {
        return $this->edit($this->findModel($id));
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);
        if ($model && $model->delete()) {
            return $this->redirect(['index']);
        }
        return '';
    }

    /**
     * @param $id
     * @return \app\modules\bank\models\Level
     * @throws NotFoundHttpException
     */
    protected function findModel($id) {
        if (($model = Level::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
