<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\bank\controllers;


use app\behaviors\Access;
use app\modules\bank\models\Node;
use app\modules\bank\models\Type;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class TypeController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => Access::class,
                'manager' => ['index', 'view', 'update', 'delete'],
            ]
        ];
    }

    public function actionIndex() {
        return $this->render('index', static::index());
    }

    public function actionAdmin() {
        return $this->render('admin', [
            'dataProvider' => new ActiveDataProvider([
                'query' => Type::find()
            ])
        ]);
    }

    public static function index() {
        return [
            'models' => Type::find()->orderBy(['price' => SORT_ASC])->all()
        ];
    }

    public function actionOpen($id) {
        $model = new Node([
            'type_id' => $id
        ]);
        $type = $model->type;
        if ($type && Yii::$app->request->isPost) {
            $amount = $type->price;
            /** @var \app\models\User $me */
            $me = Yii::$app->user->identity;
            $model->user_name = $me->name;
            if ($amount <= $me->account) {
                $transaction = Yii::$app->db->beginTransaction();
                $me->account -= $amount;
                if ($me->save() && $model->save()) {
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Success'));
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', $model->__debuginfo());
                }

                return $this->redirect(['/bank/node/view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Insufficient funds'));
            }
        }

        return $this->render('view', [
            'model' => $type,
        ]);
    }

    protected function edit(Type $model) {
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }
        return $this->render('edit', [
            'model' => $model
        ]);
    }

    public function actionCreate() {
        return $this->edit(new Type());
    }

    public function actionUpdate($id) {
        return $this->edit($this->findModel($id));
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);
        if ($model && $model->delete()) {
            return $this->redirect(['index']);
        }
        return '';
    }

    /**
     * @param $id
     * @return \app\modules\bank\models\Type
     * @throws NotFoundHttpException
     */
    protected function findModel($id) {
        if (($model = Type::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
