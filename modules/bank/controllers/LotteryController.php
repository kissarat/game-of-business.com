<?php

namespace app\modules\bank\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\modules\bank\models\Node;
use PDO;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class LotteryController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'participate' => ['post']
                ],
            ],

            'access' => [
                'class' => Access::class,
                'plain' => ['view', 'participate']
            ]
        ];
    }

    public function actionIndex() {
        $lots = SQL::queryAll('
        SELECT id, "name", income,
          round("type"."price") as "price",
          round(("type".price) / 10, 2) as "stake",
          count(number) as "count",
          (10 - count(number)) as "left"
        FROM stake RIGHT JOIN "type" ON type_id = id
        WHERE node_id IS NULL
        GROUP BY id
        ORDER BY id
        ');

        return $this->render('index', [
            'lots' => $lots
        ]);
    }

    public function actionView($id) {
        $lot = $this->findModel($id);

        $participants = SQL::queryAll('
        SELECT "number", user_name, "time" FROM stake
        WHERE type_id = :id AND node_id IS NULL
        ORDER BY number',
            [':id' => $id], PDO::FETCH_UNIQUE);

        return $this->render('view', [
            'lot' => $lot,
            'participants' => $participants
        ]);
    }

    public function actionParticipate($id, $number) {
        $lot = $this->findModel($id);
        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;

        if ($lot['price'] <= $user->account) {
            $transaction = Yii::$app->db->beginTransaction();
            $user->account -= $lot['price'];
            SQL::query('INSERT INTO stake(type_id, user_name, number) VALUES (:type_id, :participant, :number)', [
                ':type_id' => $id,
                ':participant' => $user->name,
                ':number' => $number
            ]);

            $lot = $this->findModel($id);
            if ($lot['left'] <= 0) {
                $winner = SQL::queryCell('
                  SELECT user_name FROM stake WHERE type_id = :type_id AND node_id IS NULL
                  ORDER BY random() LIMIT 1', [
                    ':type_id' => $id
                ]);
                $node = new Node([
                    'type_id' => $id,
                    'user_name' => $winner
                ]);
                if ($node->save() && $user->save()) {
                    SQL::query('UPDATE stake SET node_id = :node_id WHERE type_id = :type_id AND node_id IS NULL', [
                        ':type_id' => $id,
                        ':node_id' => $node->id,
                    ]);
                    $transaction->commit();
                    if ($winner == $user->name) {
                        Yii::$app->session->setFlash('success', 'Поздравляем! Выиграли предприятие!');
                        return $this->redirect(['/bank/node/view', 'id' => $node->id]);
                    }
                    else {
                        Yii::$app->session->setFlash('warning', 'К сожелению вы не выиграли предприятие');
                        return $this->redirect(['/bank/lottery/index']);
                    }
                }
                else {
                    $transaction->rollBack();
                    Yii::error($node->__debuginfo(), 'validation');
                    Yii::$app->session->setFlash('error', 'Невозможно сохранить предприятие');
                }
            }
            elseif ($user->save(false)) {
                Yii::$app->session->setFlash('info', 'Ставка сделана');
                $transaction->commit();
            }
            else {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', 'Невозможно сохранить баланс пользователя');
            }
        }
        else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Insufficient funds'));
        }
        return $this->redirect(['/bank/lottery/view', 'id' => $id]);
    }

    /**
     * @param integer $id
     * @return array
     * @throws NotFoundHttpException
     */
    protected function findModel($id) {
        $lot = SQL::querySingle('
        SELECT id, "name", income, "type".price,
          ("type".price) / 10 as "stake",
          ("type".price * "income") / 30 as "day",
          ("type".price * "income") / (30 * 24) as "hour",
          count(number) as "count",
          (10 - count(number)) as "left"
        FROM stake RIGHT JOIN "type" ON type_id = id
        WHERE id = :id AND node_id IS NULL
        GROUP BY id',
            [':id' => $id]);
        if (!is_null($lot)) {
            return $lot;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
