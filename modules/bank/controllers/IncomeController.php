<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\bank\controllers;
use app\modules\bank\models\Income;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use app\modules\bank\models\search\Income as IncomeSearch;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class IncomeController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ]
            ],

            'cache' => [
                'class' => 'yii\filters\HttpCache',
                'cacheControlHeader' => 'must-revalidate, private',
                'only' => ['index'],
                'enabled' => 'main' == Yii::$app->layout,
                'etagSeed' => function ($action, $params) {
                    $query = Income::find();
                    if (isset($params['user'])) {
                        $query->where(['user_name' => $params['user']]);
                    }
                    return $query->max('id');
                },
            ],
        ];
    }

    public function actionIndex($user = null) {
        $searchModel = new IncomeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'user' => $user
        ]);
    }
}
