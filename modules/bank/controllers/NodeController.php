<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\bank\controllers;


use app\behaviors\Access;
use app\helpers\Account;
use app\helpers\SQL;
use app\modules\bank\models\Node;
use app\modules\bank\models\search\Income;
use PDO;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class NodeController extends Controller
{
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'upgrade' => ['post'],
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => Access::class,
                'plain' => ['open'],
                'manager' => ['create', 'update', 'delete']
            ]
        ];
    }

    public function actionIndex($user = null, $min = 0) {
        $query = Node::find()->with('type');
        if ($user) {
            $query->andWhere(['user_name' => $user]);
        }
        else {
            $query->andWhere('"time" < :time', [
                ':time' => time() - $min * 3600 * 24
            ]);
        }
        return $this->render('index', [
            'min' => $min,
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                        'time' => SORT_ASC,
                        'id' => SORT_ASC,
                    ]
                ]
            ]),
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }

    public function actionCreate($id = null) {
        $model = new Node();
        if ($id) {
            $base = $this->findModel($id);
            $model->time = $base->time - 1;
        }
        else {
            $model->time = time();
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save(false)) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            else {
                Yii::$app->session->setFlash('error', $model->__debuginfo());
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpgrade($id) {
        /**
         * @var \app\models\User $me
         * @var \app\modules\bank\models\Type $type
         * @var \app\modules\bank\models\Level $level
         */
        $me = Yii::$app->user->identity;
        $model = $this->findModel($id);
        $type = $model->type;
        $levels = $type->getLevels();

        if ($levels->count() > 0) {
            $level = null;
            if (!$model->level_number) {
                $level = $type->getLevel(1);
            }
            else {
                $level = $model->level->getNext();
            }

            if ($level) {
                $transaction = Yii::$app->db->beginTransaction();
                if ($me->account >= $level->price) {
                    $me->account -= $level->price;
                    $model->level_number = $level->number;
                    if ($me->save(false) && $model->save(true, ['level_number'])) {
                        $transaction->commit();
                        $name = $level->type->name;
                        $percent = $level->income * 100;
                        Yii::$app->session->setFlash('success', "Предприятия $name улучшено на $percent");
                        return $this->redirect(['/user/cabinet']);
                    }
                    else {
                        $transaction->rollBack();
                        Yii::error($model->__debuginfo(), 'validation');
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Невозможно сохранить баланс пользоватлея'));
                    }
                }
                else {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Insufficient funds'));
                }
            }
            else {
                Yii::$app->session->setFlash('error', 'Уровень недоступен');
            }
        }
        else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Уровней не существует'));
        }

        return $this->redirect(['/bank/node/view', 'id' => $id]);
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Type model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Node the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Node::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
