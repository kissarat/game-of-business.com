<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\bank\models;


use Yii;
use app\helpers\SQL;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $name
 * @property number $price
 * @property float $income
 *
 * @property \app\modules\bank\models\Level[] $levels
 */
class Type extends ActiveRecord {

    public function rules() {
        return [
            ['name', 'string'],
            ['price', 'integer'],
            ['income', 'number'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'price' => Yii::t('app', 'Price'),
            'income' => Yii::t('app', 'Income')
        ];
    }

    public function getDailyIncome() {
        return $this->income * $this->price / 30;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevels() {
        return $this->hasMany(Level::class, ['type_id' => 'id']);
    }

    public function getLevel($number = 1) {
        return Level::findOne([
            'type_id' => $this->id,
            'number' => $number
        ]);
    }

    public function getTypeLevel($number) {
        return SQL::queryCell('SELECT sum(income) FROM type_level WHERE type_id = :type_id AND number <= :number', [
            ':type_id' => $this->id,
            ':number' => $number
        ]);
    }
}
