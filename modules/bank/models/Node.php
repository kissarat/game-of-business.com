<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\bank\models;

use app\helpers\Account;
use app\models\Message;
use app\models\User;
use Exception;
use Yii;
use yii\db\ActiveRecord;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * This is the model class for table "node".
 *
 * @property integer $id
 * @property string $user_name
 * @property number $type_id
 * @property integer $level_number
 * @property integer $time
 *
 * @property User $user
 * @property Type $type
 * @property Level $level
 */
class Node extends ActiveRecord
{
    public static function tableName() {
        return 'node';
    }

    public function rules() {
        return [
            [['user_name', 'datetime'], 'required'],
            [['user_name'], 'string', 'max' => 24],
            ['type_id', 'integer'],
            ['time', 'string']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_name' => Yii::t('app', 'Username'),
            'time' => Yii::t('app', 'Time'),
        ];
    }

    public function scenarios() {
        return [
            'default' => []
        ];
    }

    /**
     * @return User
     */
    public function getUser() {
        return $this->hasOne(User::class, ['name' => 'user_name']);
    }

    public function setUser(User $value) {
        $this->user_name = $value->name;
    }

    public function __debuginfo() {
        $bundle = [
            'id' => $this->id,
            'time' => date($this->time),
            'user_name' => $this->user_name
        ];
        if (count($this->errors) > 0) {
            $bundle['errors'] = $this->errors;
        }
        if (count($this->user->errors) > 0) {
            $bundle['user_errors'] = $this->user->errors;
        }
        return json_encode($bundle, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

    public function __toString() {
        return '#' . $this->id . ' ' . $this->type->name;
    }

    public function getDatetime() {
        return date('Y-m-d H:i', $this->time);
    }

    public function setDatetime($value) {
        $this->time = strtotime($value);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType() {
        return $this->hasOne(Type::class, ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel() {
        return $this->hasOne(Level::class, [
            'type_id' => 'type_id',
            'number' => 'level_number'
        ]);
    }
}
