<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\bank\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * This is the model class for table "level".
 *
 * @property integer $id
 * @property integer $number
 * @property integer $price
 * @property float $income
 * @property number $type_id
 *
 * @property Type $type
 * @property Node[] $nodes
 */
class Level extends ActiveRecord {

    public function rules() {
        return [
            ['type_id', 'integer'],
            ['number', 'integer'],
            ['price', 'integer'],
            ['income', 'number'],
        ];
    }

    public function getType() {
        return $this->hasOne(Type::class, ['id' => 'type_id']);
    }

    public function getNodes() {
        return $this->hasMany(Node::class, [
            'type_id' => 'type_id',
            'level_number' => 'number'
        ]);
    }

    public function getNext() {
        return $this->findOne([
            'type_id' => $this->type_id,
            'number' => $this->number + 1
        ]);
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_id' => Yii::t('app', 'Предприятие'),
            'number' => Yii::t('app', 'Number'),
            'price' => Yii::t('app', 'Price'),
            'income' => Yii::t('app', 'Income')
        ];
    }
}
