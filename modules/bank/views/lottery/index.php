<?php
/** @var array $lots */
use app\modules\bank\models\Type;

foreach($lots as $lot) {
    $lot['model'] = new Type([
        'id' => $lot['id'],
        'name' => $lot['name'],
        'price' => $lot['price'],
        'income' => $lot['income'],
    ]);

    echo Yii::$app->view->render('@app/modules/bank/views/type/view', $lot);
}
