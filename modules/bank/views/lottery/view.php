<?php
use yii\helpers\Html;

?>
<div class="lot">
    <div class="firm">
        <div class="title">
            <span class="name"><?= $lot['name'] ?></span>
            <div>
                <span><?= (int) $lot['stake'] ?></span>
                <img src="/images/money.png" />
            </div>
        </div>
        <div>
            <img src="/images/firm.jpg" />
            <div class="summary">
                <ul>
                    <li>
                        СТОИМОСТЬ:
                        <span><?= (int) $lot['price'] ?> монет</span>
                    </li>
                    <li>
                        ПРИБЫЛЬ:
                        <span><?= 100 * $lot['income'] ?>% в месяц</span>
                    </li>
                    <li>
                        В ДЕНЬ:
                        <span><?= floor($lot['day'] * 1000) / 1000 ?> монет</span>
                    </li>
                    <li>
                        В ЧАС:
                        <span><?= floor($lot['hour'] * 1000) / 1000 ?> монет</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?php
    /** @var array $participants */
    for($i = 1; $i <= 10; $i++) {
        $p = isset($participants[$i]) ? $participants[$i] : false;
        $options = ['class' => 'participant'];
        if ($p) {
            $options['class'] .= ' busy';
        }
        $span = Html::tag('span', $p ? $p['user_name'] : 'Сделать ставку');
        $options['data-method'] = 'post';
        $content = Html::tag('div', $i) . $span;
        if ($p) {
            echo Html::tag('div', $content, $options);
        }
        else {
            echo Html::a($content,
                ['/bank/lottery/participate',
                    'id' => $lot['id'], 'number' => $i], $options);
        }
    }
    ?>
</div>
