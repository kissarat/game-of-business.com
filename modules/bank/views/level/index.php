<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
use app\modules\bank\models\Level;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = $firm->name;
$this->params['breadcrumbs'][] = ['label' => 'Предприятие', 'url' => ['/bank/type/admin']];
$this->params['breadcrumbs'][] = $this->title;

echo Html::tag('div', Html::a('Добавить уровень',
    ['create', 'type_id' => $firm->id],
    ['class' => 'btn btn-primary']),
    ['class' => 'form-group']);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'type_id',
            'value' => function(Level $level) {
                return $level->type->name;
            }
        ],
        'number',
        'price:integer',
        [
            'attribute' => 'income',
            'value' => function(Level $model) {
                return ($model->income * 100) . '%';
            }
        ],
        ['class' => ActionColumn::class]
    ]
]);
