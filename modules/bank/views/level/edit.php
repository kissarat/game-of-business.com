<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \app\modules\bank\models\Level $model */
$this->title = 'Предприятие';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/bank/level/index', 'type_id' => $model->type_id]];
$this->params['breadcrumbs'][] = $model->isNewRecord
    ? Yii::t('app', 'Create')
    : $model->type->name . ' ' . $model->number;
$form = ActiveForm::begin();
echo Html::activeHiddenInput($model, 'type_id');
echo $form->field($model, 'number');
echo $form->field($model, 'price');
echo $form->field($model, 'income');
echo Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Create' : 'Update'), ['class' => 'btn btn-primary']);
ActiveForm::end();
