<?php
/** @var \app\modules\bank\models\Type[] $models */
?>
<div class="catalog">
    <?php foreach($models as $model) {
        echo Yii::$app->view->render('view', [
            'model' => $model
        ]);
    } ?>
</div>
