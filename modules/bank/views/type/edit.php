<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \app\modules\bank\models\Type $model */
$this->title = 'Предприятие';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['admin']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? Yii::t('app', 'Create') : $model->name;
$form = ActiveForm::begin();
echo $form->field($model, 'name');
echo $form->field($model, 'price');
echo $form->field($model, 'income');
echo Html::submitButton(Yii::t('app', $model->isNewRecord ? 'Create' : 'Update'), ['class' => 'btn btn-primary']);
ActiveForm::end();
