<?php
/** @var yii\data\ActiveDataProvider $dataProvider */
use app\modules\bank\models\Type;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

echo Html::tag('div', Html::a('Создать новый тип предприятия',
    ['create'],
    ['class' => 'btn btn-primary']),
    ['class' => 'form-group']);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'name',
        'price',
        [
            'attribute' => 'income',
            'value' => function(Type $model) {
                return $model->income * 100;
            }
        ],
        [
            'format' => 'html',
            'value' => function(Type $model) {
                return Html::a('Уровни', ['/bank/level/index', 'type_id' => $model->id]);
            }
        ],
        ['class' => ActionColumn::class]
    ]
]);
