<?php
/** @var \app\modules\bank\models\Type $model */
use yii\helpers\Html;

$play = isset($stake) ? 'Играть' : 'Купить';
if (isset($node)) {
    $play = 'Улучшить';
    $model = $node->type;
}
$stake_text = isset($stake) ? 'ставка: ' . $stake : 'цена: ' . $model->price;

?>
<div class="type">
    <div class="firm">
        <div class="title">
            <span class="name"><?= $model->name ?></span>
            <?php
            if (Yii::$app->user->getIsGuest()) {
                echo Html::a(Yii::t('app', $play), ['/user/signup']);
            }
            elseif (Yii::$app->user->identity->account >= $model->price) {
                if (isset($stake)) {
                    $url = ['/bank/lottery/view', 'id' => $model->id];
                    $options = [];
                }
                elseif (isset($node)) {
                    if ($node->level_number ? $node->level->getNext() : $model->getLevels()->count() > 0) {
                        $url = ['/bank/node/upgrade', 'id' => $node->id];
                        $options = ['data-method' => 'post'];
                    }
                    else {
                        $play = 'Усовершенствовано';
                        $url = '';
                        $options = '';
                    }
                }
                else {
                    $url = ['/bank/type/open', 'id' => $model->id];
                    $options = ['data-method' => 'post'];
                }
                echo Html::a(Yii::t('app', $play), $url, $options);
            }
            else {
                echo Html::tag('span', Yii::t('app', 'Insufficient funds'), ['class' => 'insufficient']);
            }

            $income = (isset($node) && $node->level_number) ? $model->getTypeLevel($node->level_number) : $model->income;
            ?>
        </div>
        <div>
            <img src="/images/firm.jpg" />
            <div class="summary">
                <?php if (isset($left)): ?>
                    <div class="income">Стоимость: <?= $model->price ?> монет</div>
                    <span><?= floor(1000 * $model->income * $model->price / 30) / 1000 ?></span> монет в день<br/>
                    Осталось мест: <span><?= $left ?></span>
                <?php else: ?>
                    <div class="income"><?= $income * 100 ?>% в месяц</div>
                    Прибыль:<br/>
                    <span><?= floor(100 * $income * $model->price) / 100 ?></span> монет в месяц<br/>
                    <span><?= floor(1000 * $income * $model->price / 30) / 1000 ?></span> монет в день<br/>
                <?php endif ?>

            </div>
            <?php
            $level_count = $model->getLevels()->count();
            if (!isset($node) or (!$node->level_number && $level_count > 0) or ($node->level_number && $node->level->getNext())): ?>
                <div class="stake">
                <span>
                    <?php
                    if (isset($node)) {
                        $next = $node->level_number ? $node->level->getNext() : $model->getLevel();
                        echo 'Улучшить на ' . ($next->income * 100) . '% за ' . round($next->price);
                    }
                    else {
                        echo ('lottery' == Yii::$app->controller->id ? 'ставка: ' : 'цена: ') . round($model->price);
                    }?>
                </span>
                    <img src="/images/money.png" />
                </div>
            <?php endif ?>
        </div>
    </div>
</div>
