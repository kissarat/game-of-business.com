<?php
/**
 * @link http://zenothing.com/
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\bank\models\Node */

$this->title = $model->__toString();
$url = ['index'];
if (Yii::$app->user->identity->isManager()) {
    $url['min'] = 0;
}
else {
    $url['user'] = Yii::$app->user->identity->name;
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Deposits'), 'url' => $url];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="node-view">

    <h1 class="bagatelle"><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => Yii::t('app', 'Name'),
                'attribute' => 'type_id',
                'value' => $model->type->name
            ],
            [
                'label' => Yii::t('app', 'Price'),
                'attribute' => 'type_id',
                'value' => $model->type->price
            ],
            [
                'label' => Yii::t('app', 'Income'),
                'attribute' => 'type_id',
                'value' => floor(1000 * $model->type->getDailyIncome()) / 1000
            ],
            'user_name',
            'time:datetime'
        ]
    ]) ?>
</div>
