<?php
/**
 * @link http://zenothing.com/
 */

use app\helpers\Account;
use app\models\User;
use app\modules\bank\models\Node;
use app\widgets\Ext;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$user = isset($_GET['user']) ? $_GET['user'] : null;
$mine = !Yii::$app->user->getIsGuest() && (Yii::$app->user->identity->isManager() || Yii::$app->user->identity->name == $user);
$manager = !Yii::$app->user->getIsGuest() && Yii::$app->user->identity->isManager();
$title = $this->title = Yii::t('app', 'Opened Deposits');
$balance = Account::get('profit');
$columns = [
    'id',
    [
        'attribute' => 'user_name',
        'format' => 'html',
        'value' => function(Node $model) {
            return Html::a($model->user_name, ['/user/view', 'name' => $model->user_name]);
        }
    ],
    [
        'label' => Yii::t('app', 'Name'),
        'attribute' => 'type_id',
        'value' => function(Node $model) {
            return $model->type->name;
        }
    ],
    [
        'label' => Yii::t('app', 'Price'),
        'attribute' => 'type_id',
        'value' => function(Node $model) {
            return $model->type->price;
        }
    ],
    [
        'label' => Yii::t('app', 'Income'),
        'attribute' => 'type_id',
        'value' => function(Node $model) {
            return floor(1000 * $model->type->getDailyIncome()) / 1000;
        }
    ]
];

$columns[] = 'time:datetime';
$columns[] = $manager
    ? ['class' => ActionColumn::class]
    : [
        'label' => Yii::t('app', 'Action'),
        'format' => 'html',
        'value' => function(Node $model) {
            return Html::a(Yii::t('app', 'View'), ['view', 'id' => $model->id],
                ['class' => 'btn btn-primary btn-xs']);
        }
    ];

?>
<div class="node-index">
    <?= Ext::stamp() ?>
    <?php
    if ($user) {
        echo Yii::$app->view->render('@app/views/user/panel', [
            'model' => User::findOne(['name' => $user])
        ]);
    }
    ?>
    <div class="right">
        <h1><?= $title ?></h1>

        <div class="form-group">
        <?php
        if (!Yii::$app->user->getIsGuest()) {
            if (Yii::$app->user->identity->isManager()) {
                echo Html::tag('div',
                    Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-primary']),
                    ['class' => 'form-group']);
                echo implode("\n", [
                    Html::beginForm(null, 'get'),
                    Html::label(Yii::t('app', 'Days')),
                    Html::textInput('min', $min),
                    Html::submitButton(Yii::t('app', 'Show')),
                    Html::endForm()
                ]);
            }
            else {
//                echo Html::a(Yii::t('app', 'Open'), ['open'], ['class' => 'btn btn-success']);
            }
        }
        ?>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'summary' => '',
            'emptyText' => '',
            'showOnEmpty' => false,
            'columns' => $columns,
        ])
        ?>
    </div>

</div>
