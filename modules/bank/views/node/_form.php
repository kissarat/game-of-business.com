<?php
/**
 * @link http://zenothing.com/
 */

use app\widgets\AjaxComplete;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\bank\models\Node */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    if (Yii::$app->user->identity->isManager()) {
        echo $form->field($model, 'user_name')->widget(AjaxComplete::class, [
        'route' => ['/user/complete']
    ]);
        echo Html::activeHiddenInput($model, 'time');
    } ?>
    <?= $form->field($model, 'amount') ?>
    <?= $form->field($model, 'datetime')->widget(DateTimePicker::class) ?>

    <div class="form-group">
        <?php
        if (Yii::$app->user->identity->isManager()) {
            echo Html::submitButton($model->isNewRecord
                ? Yii::t('app', 'Create')
                : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
        }
        else {
            echo Html::submitButton(Yii::t('app', 'Open'), ['class' => 'btn btn-success']);
        }
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
