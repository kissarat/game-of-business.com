<?php
/** @var \app\modules\bank\models\Node[] $firms */
?>
<div class="catalog">
    <?php foreach($firms as $node) {
        echo Yii::$app->view->render('@app/modules/bank/views/type/view', [
            'node' => $node
        ]);
    } ?>
</div>
