"use strict";
var pg = require('pg');
var Server = require('ws').Server;
var fs = require('fs');
var source = fs.readFileSync(__dirname + '/config/local.php', 'utf8').replace(/\s+/g, ' ');
var password = /'password'[^']+'([^']+)'/.exec(source);
if (password) {
    password = password[1];
}
else {
    console.error('local.php not found');
    process.exit();
}

var server = new Server({port: 5000});
var sockets = new Map();

function address(socket) {
    return socket._socket.remoteAddress + ':' + socket._socket.remotePort;
}

server.on('connection', function(socket) {
    socket.id = address(socket);
    sockets.set(socket.id, socket);
});

pg.connect(`postgres://game-of-business.com:${password}@localhost/game-of-business.com`, function(err, client) {
    if(err) {
        console.log(err);
    }

    client.on('notification', function(message) {
        sockets.forEach(function(socket) {
            if (socket.OPEN == socket.readyState) {
                socket.send(message.payload)
            }
            else {
                sockets.delete(socket.id);
            }
        });
    });
    var venture = client.query('LISTEN venture');
    var stake = client.query('LISTEN stake');
});
