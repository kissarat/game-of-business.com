/* Yii log table using for debug only */
CREATE TABLE "log" (
  "id"          SERIAL NOT NULL PRIMARY KEY,
  "level"       INT,
  "category"    VARCHAR(256),
  "log_time"    DOUBLE PRECISION,
  "prefix"      TEXT,
  "message"     TEXT
);


/* Yii session table (not in use) */
CREATE TABLE "session" (
  id CHAR(40) NOT NULL PRIMARY KEY,
  expire INT,
  data BYTEA
);


/* Yii cache storage table (not in use) */
CREATE TABLE "cache" (
  id CHAR(128) NOT NULL PRIMARY KEY,
  expire INT,
  data BYTEA
);


/* Yii English source translation table */
/* app\modules\lang\controllers\TranslationController uses this table */
CREATE TABLE "source_message" (
  id SERIAL PRIMARY KEY,
  category VARCHAR(32) DEFAULT 'app',
  message VARCHAR(256)
);
CREATE UNIQUE INDEX message_id ON "source_message" USING btree ("id");


/* Yii Russian target translation table */
/* app\modules\lang\controllers\TranslationController uses this table */
CREATE TABLE "message" (
  "id" INT,
  "language" VARCHAR(16) DEFAULT 'ru',
  "translation" VARCHAR(256),
  PRIMARY KEY (id, language),
  CONSTRAINT fk_message_source_message FOREIGN KEY (id)
  REFERENCES source_message (id) ON DELETE CASCADE ON UPDATE RESTRICT
);


/* English-Russian translation view */
CREATE VIEW "translation" AS
  SELECT s.id, message, translation
  FROM source_message s JOIN message t ON s.id = t.id
  WHERE "language" = 'ru';


/* User account, see app\models\User */
CREATE TABLE "user" (
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(24) NOT NULL UNIQUE,
  account DECIMAL(8,2) NOT NULL DEFAULT '0.00',
  email VARCHAR(48) NOT NULL UNIQUE,
  hash CHAR(60),
  auth CHAR(64) UNIQUE,
  code CHAR(64),
  status SMALLINT NOT NULL DEFAULT 2,
  forename VARCHAR(24),
  surname VARCHAR(24),
  patronymic VARCHAR(24),
  phone VARCHAR(16),
  perfect VARCHAR(9) NOT NULL,
  ref_name VARCHAR(24),
  last_access TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  data BYTEA
);
CREATE UNIQUE INDEX user_id ON "user" USING btree ("id");
CREATE UNIQUE INDEX user_name ON "user" USING btree ("name");
CREATE UNIQUE INDEX user_email ON "user" USING btree ("email");

INSERT INTO "user"(name, email, status, perfect) VALUES ('admin', 'lab_tas@ukr.net', 1, 'U9525021');


/* User action log, see app\models\Record */
CREATE TABLE "journal" (
  id SERIAL PRIMARY KEY NOT NULL,
  type VARCHAR(16) NOT NULL,
  event VARCHAR(16) NOT NULL,
  object_id INT,
  data TEXT,
  user_name VARCHAR(24),
  time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  ip INET,
  CONSTRAINT journal_user FOREIGN KEY (user_name)
  REFERENCES "user"(name)
  ON DELETE CASCADE ON UPDATE CASCADE
);


/* Payment (if amount > 0) and withdrawal (if amount < 0) table, see app\invoice\models\Invoice */
CREATE TABLE "invoice" (
  id SERIAL PRIMARY KEY NOT NULL,
  user_name VARCHAR(24) NOT NULL,
  amount DECIMAL(8,2) NOT NULL,
  perfect CHAR(8) NOT NULL,
  batch BIGINT,
  status VARCHAR(16) DEFAULT 'create',
  CONSTRAINT invoice_user FOREIGN KEY (user_name)
  REFERENCES "user"(name)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT amount CHECK (amount <> 0)
);
CREATE UNIQUE INDEX invoice_id ON "invoice" USING btree ("id");


/* Site balance */
CREATE TABLE "account" (
  "profit" NUMERIC(8,2) NOT NULL DEFAULT 0
);
INSERT INTO "account" VALUES (0);


CREATE TABLE "type" (
  id SERIAL PRIMARY KEY NOT NULL,
  "name" VARCHAR(48) NOT NULL,
  price NUMERIC(8,2) NOT NULL,
  income FLOAT NOT NULL,
  CONSTRAINT price CHECK (price > 0),
  CONSTRAINT income CHECK (income > 0)
);

INSERT INTO "type" VALUES (1, 'Кафе', 100, 0.20);
INSERT INTO "type" VALUES (2, 'Казино', 250, 0.25);
INSERT INTO "type" VALUES (3, 'Ночной клуб', 500, 0.27);
INSERT INTO "type" VALUES (4, 'Радиостанция', 1500, 0.33);
INSERT INTO "type" VALUES (5, 'Пивоварня', 3000, 0.35);
INSERT INTO "type" VALUES (6, 'Табачная фабрика', 10000, 0.37);
INSERT INTO "type" VALUES (7, 'Нефтяная компания', 15000, 0.40);
INSERT INTO "type" VALUES (8, 'Банк', 30000, 0.60);


CREATE TABLE "level" (
  id SERIAL PRIMARY KEY NOT NULL,
  type_id INT NOT NULL,
  number SMALLINT,
  price NUMERIC(8,2) NOT NULL DEFAULT 0,
  income FLOAT NOT NULL DEFAULT 0,
  UNIQUE(type_id, number),
  CONSTRAINT price CHECK (price > 0),
  CONSTRAINT income CHECK (income > 0),
  CONSTRAINT level_type FOREIGN KEY (type_id) REFERENCES "type"(id)
  ON DELETE CASCADE ON UPDATE CASCADE
);



CREATE VIEW type_level AS
  SELECT type_id, price, income, ((price * income) / 30) as daily, number FROM
    (
      SElECT id as type_id, price, income, 0 as number FROM "type"
      UNION
      SELECT type_id, price, income, number FROM "level"
    ) i;


/* Investments table, see app\modules\bank\models\Node */
CREATE TABLE "node" (
  id SERIAL PRIMARY KEY NOT NULL,
  type_id INT NOT NULL,
  user_name VARCHAR(24) NOT NULL,
  level_number SMALLINT,
  "time" TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT node_type FOREIGN KEY ("type_id")
  REFERENCES "type"(id),
  CONSTRAINT node_user FOREIGN KEY (user_name)
  REFERENCES "user"(name)
);
CREATE UNIQUE INDEX node_id ON "node" USING btree ("id");


CREATE TABLE "stake" (
  type_id INT NOT NULL,
  user_name VARCHAR(24) NOT NULL,
  number SMALLINT NOT NULL,
  node_id INT,
  "time" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT stake_type FOREIGN KEY ("type_id")
  REFERENCES "type"(id),
  CONSTRAINT stake_user FOREIGN KEY (user_name)
  REFERENCES "user"(name),
  CONSTRAINT stake_node FOREIGN KEY (node_id)
  REFERENCES "node"(id),
  UNIQUE(type_id, number, node_id),
  UNIQUE("time")
);


/* Income from investments log, see app\modules\bank\models\Income */
CREATE TABLE "income" (
  id SERIAL PRIMARY KEY NOT NULL,
  node_id INT NOT NULL,
  "time" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT income_node FOREIGN KEY ("node_id")
  REFERENCES "node"(id)
);
CREATE UNIQUE INDEX income_id ON "income" USING btree ("id");


/* User and guest feedback table for app\modules\feedback\models\Feedback */
CREATE TABLE "feedback" (
  id SERIAL PRIMARY KEY NOT NULL,
  username VARCHAR(24) NOT NULL,
  email VARCHAR(48),
  subject VARCHAR(256) NOT NULL,
  content TEXT NOT NULL
);
CREATE UNIQUE INDEX feedback_id ON "feedback" USING btree ("id");


CREATE TABLE "note" (
  id SERIAL PRIMARY KEY NOT NULL,
  sender VARCHAR(24),
  receiver VARCHAR(24) NOT NULL,
  type VARCHAR(16) NOT NULL,
  link VARCHAR(80),
  content TEXT NOT NULL,
  time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT sender FOREIGN KEY (sender)
  REFERENCES "user"(name),
  CONSTRAINT receiver FOREIGN KEY (receiver)
  REFERENCES "user"(name)
);


CREATE TABLE "article" (
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR(24),
  "title" VARCHAR(256) NOT NULL,
  "keywords" VARCHAR(192),
  "summary" TEXT,
  "content" TEXT NOT NULL
);
CREATE UNIQUE INDEX article_id ON "article" USING btree ("id");
INSERT INTO article(name, title, content) VALUES
  ('about', 'Бизнес-приключения', ''),
  ('marketing', 'Маркетинг', ''),
  ('rules', 'Правила', ''),
  ('contacts', 'О нас', '');

CREATE TABLE "review" (
  id SERIAL PRIMARY KEY NOT NULL,
  user_name VARCHAR(24) NOT NULL,
  content TEXT NOT NULL,
  CONSTRAINT review_user FOREIGN KEY (user_name)
  REFERENCES "user"(name)
  ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX review_id ON "review" USING btree ("id");


CREATE TABLE "faq" (
  "id" SERIAL PRIMARY KEY NOT NULL,
  "question" VARCHAR(256) NOT NULL,
  "answer" TEXT NOT NULL
);


CREATE TABLE "block" (
  ip INET PRIMARY KEY NOT NULL,
  reason TEXT,
  time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE "setting" (
  "category" VARCHAR(24) NOT NULL,
  "name" VARCHAR(24) NOT NULL,
  "value" TEXT
);

INSERT INTO "setting" VALUES ('common', 'email', NULL);

INSERT INTO "setting" VALUES ('perfect', 'id', NULL);
INSERT INTO "setting" VALUES ('perfect', 'password', NULL);
INSERT INTO "setting" VALUES ('perfect', 'wallet', NULL);
INSERT INTO "setting" VALUES ('perfect', 'secret', NULL);

INSERT INTO "setting" VALUES ('account', 'balance', '100');
INSERT INTO "setting" VALUES ('account', 'min_payment', '300');
INSERT INTO "setting" VALUES ('account', 'max_withdraw', '3000');
INSERT INTO "setting" VALUES ('account', 'vip_bonus', '0.12');
INSERT INTO "setting" VALUES ('account', 'bonus', '0.05');
INSERT INTO "setting" VALUES ('account', 'rate', '60');
INSERT INTO "setting" VALUES ('account', 'return', '0.05');
INSERT INTO "setting" VALUES ('account', 'vip', '5000');
INSERT INTO "setting" VALUES ('account', 'vip_duration', '30');


CREATE TABLE "vip" (
  "id" SERIAL PRIMARY KEY NOT NULL,
  "user_name" VARCHAR(24) NOT NULL,
  "start" TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  "end" TIMESTAMP NOT NULL,
  CONSTRAINT vip_user FOREIGN KEY (user_name)
  REFERENCES "user"(name)
  ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE VIEW "vip_view" AS
  SELECT id, user_name, "start", ("end"::date - "start"::date) as "days" FROM "vip";

CREATE OR REPLACE RULE vip_view_insert AS ON INSERT TO "vip_view" DO INSTEAD
  INSERT INTO "vip"("id", "user_name", "end")
  VALUES (nextval('vip_id_seq'), NEW."user_name", CURRENT_TIMESTAMP::date + NEW."days");

CREATE OR REPLACE RULE vip_view_delete AS ON DELETE TO "vip_view" DO INSTEAD
  DELETE FROM "vip" WHERE "id" = OLD.id;


CREATE TABLE "bonus" (
  "start" INT NOT NULL,
  "end" INT NOT NULL,
  "value" FLOAT NOT NULL
);

INSERT INTO "bonus" VALUES (1, 999, 0.025);
INSERT INTO "bonus" VALUES (1000, 2999, 0.05);
INSERT INTO "bonus" VALUES (3000, 4999, 0.15);
INSERT INTO "bonus" VALUES (5000, 9999, 0.20);
INSERT INTO "bonus" VALUES (10000, 50000, 0.25);


CREATE TABLE "competition" (
  "time" TIMESTAMP DEFAULT CURRENT_TIMESTAMP PRIMARY KEY NOT NULL
);

INSERT INTO "competition" DEFAULT VALUES;


CREATE TABLE "prize" (
  "number" INT PRIMARY KEY,
  "value" INT NOT NULL
);

INSERT INTO "prize" VALUES (1, 150000);
INSERT INTO "prize" VALUES (2, 100000);
INSERT INTO "prize" VALUES (3, 50000);
INSERT INTO "prize" VALUES (4, 25000);
INSERT INTO "prize" VALUES (5, 10000);


CREATE TABLE "venture" (
  "id" SERIAL PRIMARY KEY NOT NULL,
  "stake" SMALLINT NOT NULL,
  "status" SMALLINT NOT NULL DEFAULT 1
);


CREATE TABLE "venture_user" (
  "venture_id" INT NOT NULL,
  user_name VARCHAR(24) NOT NULL,
  "time" TIMESTAMP DEFAULT current_timestamp
);


CREATE VIEW venture_view AS
  SELECT id, stake, status, user_name, "time" FROM venture JOIN venture_user ON id = venture_id;


/* see web/visit.php */
/* Visitors list */
CREATE TABLE "visit_agent" (
  "id" SERIAL PRIMARY KEY,
  "agent" VARCHAR(200),
  "ip" INET
);
CREATE INDEX visit_agent_agent ON "visit_agent" USING btree ("agent");
CREATE INDEX visit_agent_ip ON "visit_agent" USING btree ("ip");


/* Visitors log */
CREATE TABLE "visit_path" (
  "id" SERIAL PRIMARY KEY,
  "agent_id" INT NOT NULL,
  "path" VARCHAR(80) NOT NULL,
  "spend" SMALLINT,
  "time" TIMESTAMP DEFAULT current_timestamp,
  CONSTRAINT user_agent FOREIGN KEY (agent_id)
  REFERENCES "visit_agent"("id")
  ON DELETE CASCADE ON UPDATE CASCADE
);


/* Visitors with user agent log view */
CREATE VIEW "visit" AS
  SELECT p.id as id, agent_id, spend, user_name, "path", p."time", a.ip, agent FROM visit_path p
    JOIN visit_agent a ON agent_id = a.id
    LEFT JOIN journal j ON a.ip = j.ip and user_name is not null
    GROUP BY p.id, user_name, a.ip, agent;
