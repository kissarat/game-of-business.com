<?php
/**
 * @link http://zenothing.com/
 */

if (empty($_COOKIE['lang'])) {
    $language = empty($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? '' : $_SERVER['HTTP_ACCEPT_LANGUAGE'];
    $language = preg_match('/(ru|uk|be|ky|ab|mo|et|lv)/i', $language) ? 'ru' : 'en';
}
else {
    $language = $_COOKIE['lang'];
}

$admin = isset($_SERVER['HTTP_HOST']) && false !== strpos($_SERVER['HTTP_HOST'], 'admin');

$config = [
    'id' => 'china-stroyinvest',
    'name' => 'Строй-ИНВЕСТ',
    'basePath' => __DIR__ . '/..',
    'bootstrap' => ['log'],
    'defaultRoute' => 'home/' . ($admin ? 'statistics' : 'index'),
    'language' => $language,
    'charset' => 'utf-8',
    'layout' => $admin ? 'admin' : 'main',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\ApcCache'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                    'enabled' => false
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                'investments/user/<user:[\w_\-\.]+>' => 'bank/node/index',
                'investment/<id:\d+>' => 'bank/node/view',
                'income/user/<user:[\w_\-\.]+>' => 'bank/income/index',
                'open/<id:\d+>' => 'bank/type/open',
                'income' => 'bank/income/index',
                'investments' => 'bank/node/index',

                'register/<ref_name:[\w_\-\.]+>' => 'user/signup',
                'journal/user/<user:[\w_\-\.]+>' => 'journal/index',
                'feedback/template/<template:\w+>' => 'feedback/create',
                '<scenario:(withdraw|payment)>/user/<user:[\w_\-\.]+>' => 'invoice/invoice/index',
                '<scenario:(withdraw|payment)>/<amount:\d+>' => 'invoice/invoice/create',
                '<scenario:(withdraw|payment)>' => 'invoice/invoice/index',
                'invoices/user/<user:[\w_\-\.]+>' => 'invoice/invoice/index',
                'invoice/<id:\d+>' => 'invoice/invoice/view',
                'journal/<id:\d+>' => 'journal/view',
                'lot<id:\d+>' => 'bank/lottery/view',
                'stake<id:\d+>-<number:\d+>' => 'bank/lottery/participate',
//                'settings/<name:[\w_\-\.]+>' => 'user/update',
                'password/<name:[\w_\-\.]+>' => 'user/password',
                'reset/<code:[\w_\-]{64}>' => 'user/password',
                'inform/<user:[\w_\-\.]+>' => 'message/create',
                'dialog/<user:[\w_\-\.]+>' => 'message/dialog',
                'page/<name:.*>' => 'article/article/page',
                'compose/<scenario:(page|default)>' => 'article/article/create',
                'edit/<id:\d+>' => 'article/article/update',
                'visits/<user:[\w_\-\.]+>' => 'admin/visit',
                'settings/<category:[\w_\-\.]+>' => 'setting/edit',
                'top-<top:\d+>-<by:[\w_\-\.]+>' => 'home/top',
                'settings' => 'setting/index',
                'reviews' => 'review/index',
                'compose' => 'article/article/create',
                'pages' => 'article/article/pages',
                'news' => 'article/article/index',
                'login' => 'user/login',
                'register' => 'user/signup',
                'cabinet' => 'user/view',
                'payments' => 'invoice/invoice/index',
                'translations' => 'lang/lang/index',
                'profit' => 'user/account',
                'contact-us' => 'feedback/feedback/create',
                'statistics' => 'home/statistics',
                'visits' => 'admin/visit',
                'marketing' => 'bank/type/index',
                'lottery' => 'bank/lottery/index',
                '/' => 'home/index',
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'enableCaching' => true
                ]
            ]
        ],
        'session' => [
            'class' => 'yii\web\CacheSession',
            'name' => 'auth'
        ],
        'backup' => [
            'class' => 'app\components\Backup',
        ],
    ],
    'params' => null,
];
