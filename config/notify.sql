CREATE FUNCTION venture_insert() RETURNS TRIGGER AS $$
DECLARE
BEGIN
  IF (SELECT count(*) FROM venture_user WHERE venture_id = NEW.venture_id) = 1 THEN
    PERFORM pg_notify('venture', (SELECT row_to_json(v) FROM
      (SELECT vv.*, 'insert' as _method FROM venture_view vv) v WHERE id = NEW.venture_id)::TEXT);
  END IF;
  RETURN NEW;
END
$$ LANGUAGE plpgsql;

CREATE TRIGGER venture_insert AFTER INSERT ON venture_user
FOR EACH ROW EXECUTE PROCEDURE venture_insert();


CREATE FUNCTION venture_update() RETURNS TRIGGER AS $$
DECLARE
BEGIN
  IF OLD.status > NEW.status THEN
    PERFORM pg_notify('venture', (SELECT row_to_json(v) FROM
      (SELECT vv.*, 'update' as _method FROM venture vv) v WHERE id = NEW.id)::TEXT);
  END IF;
  RETURN NEW;
END
$$ LANGUAGE plpgsql;

CREATE TRIGGER venture_update AFTER UPDATE ON venture
FOR EACH ROW EXECUTE PROCEDURE venture_update();


CREATE FUNCTION stake_insert() RETURNS TRIGGER AS $$
DECLARE
BEGIN
  PERFORM pg_notify('stake', row_to_json(NEW)::TEXT);
  RETURN NEW;
END
$$ LANGUAGE plpgsql;

CREATE TRIGGER stake_insert AFTER INSERT ON stake
FOR EACH ROW EXECUTE PROCEDURE stake_insert();
