<?php
/**
 * @link http://zenothing.com/
*/
use yii\console\Application;

require_once __DIR__ . '/../config/boot.php';

$app = new Application($config);

function generateRandomWallet($length = 8) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = 'U';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$length = (int) $argv[1];
$transaction = Yii::$app->db->beginTransaction();
for($i = 2; $i < $length; $i++) {
    $name = 'user' . $i;
    $app->db->createCommand('INSERT INTO "user"("name", email, perfect) VALUES (:name, :email, :perfect)', [
        ':name' => $name,
        ':email' => $name . '@yopmail.com',
        ':perfect' => generateRandomWallet()
    ])->execute();
    $names[] = $name;
}

$app->db->createCommand('UPDATE "user" SET "account" = 1000,
"hash" = \'$2y$10$zEiSdGHD2q9fRtljNONCbuj15hjLMNTU71IaM5PcR503kNz3VfC7W\'')->execute();
$transaction->commit();
